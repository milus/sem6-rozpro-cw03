package com.milus.ex03.server.api;

import com.milus.ex03.server.api.exception.ExistingUserException;
import com.milus.ex03.server.core.rmi.RMIGameBuilder;
import lombok.NonNull;

import java.io.Serializable;
import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RMIServer extends Remote, Serializable {
    void join(String nickname) throws ExistingUserException, RemoteException;

    void disconnect(String nickname) throws RemoteException;

    RMIGameBuilder joinGame(String nickname) throws RemoteException;
}
