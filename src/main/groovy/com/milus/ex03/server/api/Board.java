package com.milus.ex03.server.api;

import com.google.common.collect.Maps;
import com.milus.ex03.server.core.Player;
import com.milus.ex03.server.core.WinningPolicy;
import lombok.NonNull;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.Map;
import java.util.Optional;

public class Board implements RMIBoard, Serializable {
    private static final long serialVersionUID = -4325158068994781329L;

    private final Player playerA;
    private final Player playerB;

    private final Map<Integer, Player> positions = Maps.newConcurrentMap();

    private transient Optional<Player> winner = Optional.empty();

    public Board(Player playerA, Player playerB) throws RemoteException {
        this.playerA = playerA;
        this.playerB = playerB;
    }

    @Override
    public Player getWinner() {
        return winner.orElse(playerA);
    }

    @Override
    public boolean isMovePossible(@NonNull Player player, @NonNull Position position) {
        if (!player.equals(playerA) && !player.equals(playerB)) {
            return false;
        }

        int value = position.getValue();
        if (value < 0 || value > 8) {
            return false;
        }

        if (positions.containsKey(value)) {
            return false;
        }

        return true;
    }

    @Override
    public void move(@NonNull Player player, @NonNull Position position) {
        if (isMovePossible(player, position)) {
            positions.put(position.getValue(), player);
            return;
        }
        throw new IllegalArgumentException("Move is not possible + " + position.getValue());
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(" | | ").append("\n").append(" | | ").append("\n").append(" | | ");

        for (Map.Entry<Integer, Player> entry : positions.entrySet()) {
            Player player = entry.getValue();
            char c;
            if (player.equals(playerA)) {
                c = 'X';
            } else {
                c = 'O';
            }
            sb.setCharAt(findPlaceInString(entry.getKey()), c);
        }
        return sb.toString();
    }

    private int findPlaceInString(int key) {
        return 2 * key;
    }

    @Override
    public boolean isFinished() {
        winner = new WinningPolicy().findWinner(positions);
        return winner.isPresent();
    }
}
