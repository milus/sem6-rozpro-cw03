package com.milus.ex03.server.api;

import com.milus.ex03.server.core.Player;
import lombok.NonNull;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RMIBoard extends Remote {
    Player getWinner() throws RemoteException;

    boolean isMovePossible(Player player, Position position) throws RemoteException;

    void move(Player player, Position position) throws RemoteException;

    boolean isFinished() throws RemoteException;
}
