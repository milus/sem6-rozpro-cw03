package com.milus.ex03.server.api.listener;

import com.milus.ex03.server.api.Position;
import com.milus.ex03.server.api.RMIBoard;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface MyTurnListener extends Remote {
    Position getPosition(RMIBoard board) throws RemoteException;
}
