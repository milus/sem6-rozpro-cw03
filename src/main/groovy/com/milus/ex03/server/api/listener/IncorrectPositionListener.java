package com.milus.ex03.server.api.listener;

import com.milus.ex03.server.api.Position;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IncorrectPositionListener extends Remote {
    void onIncorrectPosition(Position position) throws RemoteException;
}
