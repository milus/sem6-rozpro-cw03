package com.milus.ex03.server.api;

import lombok.Value;

import java.io.Serializable;

@Value
public class Position implements Serializable{
    private static final long serialVersionUID = -514829107166104990L;
    private final int value;
}
