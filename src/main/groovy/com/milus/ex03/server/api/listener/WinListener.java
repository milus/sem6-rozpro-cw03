package com.milus.ex03.server.api.listener;

import com.milus.ex03.server.api.RMIBoard;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface WinListener extends Remote{
    void onWin(RMIBoard board) throws RemoteException;
}
