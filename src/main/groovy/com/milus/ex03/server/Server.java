package com.milus.ex03.server;

import com.google.common.collect.Sets;
import com.milus.ex03.server.api.GameType;
import com.milus.ex03.server.api.RMIServer;
import com.milus.ex03.server.api.exception.ExistingUserException;
import com.milus.ex03.server.api.listener.*;
import com.milus.ex03.server.core.Computer;
import com.milus.ex03.server.core.Game;
import com.milus.ex03.server.core.PvPGame;
import com.milus.ex03.server.core.rmi.RMIGameBuilder;
import lombok.NonNull;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Optional;
import java.util.Set;

import static com.google.common.base.Preconditions.checkNotNull;

public class Server implements RMIServer {
    private static final long serialVersionUID = 4096733743714047212L;

    private final Set<String> players = Sets.newConcurrentHashSet();

    private final Set<Game> games = Sets.newConcurrentHashSet();

    @Override
    public void join(@NonNull String nickname) throws ExistingUserException {
        if (players.contains(nickname) || nickname.equals(Computer.NAME)) {
            throw new ExistingUserException();
        }
        players.add(nickname);
    }

    @Override
    public void disconnect(@NonNull String nickname) throws RemoteException {
        if (players.contains(nickname)) {
            players.remove(nickname);
        }
    }

    @Override
    public RMIGameBuilder joinGame(@NonNull String nickname) throws RemoteException {
        if (!players.contains(nickname)) {
            throw new IllegalArgumentException(nickname + " is not registered");
        }
        return new GameBuilder(nickname);
    }

    public class GameBuilder extends UnicastRemoteObject implements RMIGameBuilder, Serializable {
        private static final long serialVersionUID = 2480629723210997813L;

        private final String nickname;

        private GameType gameType;
        private MyTurnListener myTurnListener;
        private EnemyTurnListener enemyTurnListener;
        private IncorrectPositionListener incorrectPositionListener;
        private WinListener winListener;
        private LooseListener looseListener;

        public GameBuilder(String nickname) throws RemoteException {
            this.nickname = nickname;
        }

        @Override
        public RMIGameBuilder type(@NonNull GameType gameType) {
            this.gameType = gameType;
            return this;
        }

        @Override
        public RMIGameBuilder myTurnListener(@NonNull MyTurnListener listener) {
            this.myTurnListener = listener;
            return this;
        }

        @Override
        public RMIGameBuilder enemyTurnListener(@NonNull EnemyTurnListener listener) {
            this.enemyTurnListener = listener;
            return this;
        }

        @Override
        public RMIGameBuilder incorrectPositionListener(@NonNull IncorrectPositionListener listener) {
            this.incorrectPositionListener = listener;
            return this;
        }

        @Override
        public RMIGameBuilder winListener(@NonNull WinListener listener) {
            this.winListener = listener;
            return this;
        }

        @Override
        public RMIGameBuilder looseListener(@NonNull LooseListener listener) {
            this.looseListener = listener;
            return this;
        }

        @Override
        public void go() {
            if (!players.contains(nickname)) {
                throw new IllegalStateException(nickname + " has not joined to the server");
            }

            checkNotNull(gameType);
            checkNotNull(myTurnListener);
            checkNotNull(enemyTurnListener);
            checkNotNull(incorrectPositionListener);
            checkNotNull(winListener);
            checkNotNull(looseListener);

            doCleanUp();

            switch (gameType) {
                case PVE:
                    startPveGame();
                    break;
                case PVP:
                    startPvpGame();
                    break;
            }
        }

        private void startPveGame() {
            Game game = Game.pve()
                    .withPlayer()
                        .nickname(nickname)
                        .playerTurn(myTurnListener)
                        .enemyTurn(enemyTurnListener)
                        .onIncorrectMove(incorrectPositionListener)
                        .onWin(winListener)
                        .onLoose(looseListener)
                    .create();
            games.add(game);
            game.start();
        }


        private void startPvpGame() {
            Optional<Game> game = games.stream()
                    .filter(Game::waitingForPlayers)
                    .findAny();

            if (game.isPresent() && game.get() instanceof PvPGame) {
                ((PvPGame) game.get())
                        .setPlayerB()
                        .nickname(nickname)
                        .playerTurn(myTurnListener)
                        .enemyTurn(enemyTurnListener)
                        .onIncorrectMove(incorrectPositionListener)
                        .onWin(winListener)
                        .onLoose(looseListener)
                        .start();
            } else {
                Game newGame = Game.pvp()
                        .withPlayerA()
                        .nickname(nickname)
                        .playerTurn(myTurnListener)
                        .enemyTurn(enemyTurnListener)
                        .onIncorrectMove(incorrectPositionListener)
                        .onWin(winListener)
                        .onLoose(looseListener)
                        .create();
                games.add(newGame);

            }
        }
    }

    private void doCleanUp() {
        games.removeIf(Game::isFinished);
    }

    public static void main(String[] args) {
        if (args.length < 2) {
            System.err.println("Please provide IP and port");
            return;
        }
        String ip = args[0];
        String port = args[1];
        try {
            Server server = new Server();
            RMIServer remoteServer = (RMIServer) UnicastRemoteObject.exportObject(server, 0);
            String url = String.format("rmi://%s:%s/server", ip, port);
            Naming.rebind(url, remoteServer);
            System.out.println("Server started");
        } catch (RemoteException | MalformedURLException e) {
            e.printStackTrace();
        }
    }
}
