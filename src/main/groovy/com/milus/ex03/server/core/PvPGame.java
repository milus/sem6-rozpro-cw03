package com.milus.ex03.server.core;

import com.google.common.base.Throwables;
import com.milus.ex03.server.api.Board;
import com.milus.ex03.server.api.Position;
import com.milus.ex03.server.api.RMIBoard;
import com.milus.ex03.server.api.listener.*;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import rx.Observable;
import rx.schedulers.Schedulers;

import java.rmi.RemoteException;
import java.util.Optional;
import java.util.concurrent.Executors;

@RequiredArgsConstructor
public class PvPGame extends Game {

    private Optional<Board> boardOptional = Optional.empty();
    private final Player playerA;
    private Optional<Player> optionalPlayerB = Optional.empty();

    private Board board;
    private Player playerB;

    @Override
    public void start() {
        if (!boardOptional.isPresent()) {
            throw new IllegalStateException("Board cannot be empty");
        }

        if (!optionalPlayerB.isPresent()) {
            throw new IllegalStateException("Second player must have joined the game");
        }

        board = boardOptional.get();
        playerB = optionalPlayerB.get();

        Observable.create(subscriber -> subscriber.onNext(board))
                .observeOn(Schedulers.from(Executors.newSingleThreadExecutor()))
                .map(b -> firstPlayerMove())
                .map(b -> secondPlayerMove())
                .map(b -> firstPlayerMove())
                .map(b -> secondPlayerMove())
                .map(b -> firstPlayerMove())
                .map(b -> secondPlayerMove())
                .map(b -> firstPlayerMove())
                .map(b -> secondPlayerMove())
                .map(b -> firstPlayerMove())
                .subscribe(b -> {
                            finish();
                            Player winner = board.getWinner();
                            try {
                                if (winner.equals(playerA)) {
                                    playerA.onWin(board);
                                } else {
                                    playerB.onWin(board);
                                }
                            } catch (RemoteException ignored) {
                            }
                            try {
                                if (winner.equals(playerA)) {
                                    playerB.onLoose(board);
                                } else {
                                    playerA.onLoose(board);
                                }
                            } catch (RemoteException ignored) {
                            }
                        },
                        Throwable::printStackTrace);


    }

    @Override
    public boolean waitingForPlayers() {
        return !optionalPlayerB.isPresent();
    }

    private RMIBoard firstPlayerMove() {
        try {
            if (board.isFinished()) {
                return board;
            }
            doNextMove(playerA);
            playerB.onEnemyTurn(board);
        } catch (RemoteException e) {
            Throwables.propagate(e);
        }
        return board;
    }

    private RMIBoard secondPlayerMove() {
        try {
            if (board.isFinished()) {
                return board;
            }
            doNextMove(playerB);
            playerA.onEnemyTurn(board);
        } catch (RemoteException e) {
            Throwables.propagate(e);
        }
        return board;
    }

    private void doNextMove(Player player) {
        try {
            Position position = player.getPosition(board);
            while (!board.isMovePossible(player, position)) {
                player.onIncorrectMove(position);
                position = player.getPosition(board);
            }
            board.move(player, position);
        } catch (RemoteException e) {
            Throwables.propagate(e);
        }
    }

    public SecondPlayerBuilder setPlayerB() {
        return new SecondPlayerBuilder();
    }

    public static class Builder {
        public PlayerBuilder withPlayerA() {
            return new PlayerBuilder();
        }
    }

    public static class PlayerBuilder implements Game.PlayerBuilder {
        Player.PlayerBuilder playerBuilder = Player.builder();

        @Override
        public PlayerBuilder nickname(@NonNull String nickname) {
            playerBuilder.nickname(nickname);
            return this;
        }

        @Override
        public PlayerBuilder playerTurn(@NonNull MyTurnListener myTurnListener) {
            playerBuilder.myTurnListener(myTurnListener);
            return this;
        }

        @Override
        public PlayerBuilder onIncorrectMove(IncorrectPositionListener incorrectPositionListener) {
            playerBuilder.incorrectPositionListener(incorrectPositionListener);
            return this;
        }

        @Override
        public PlayerBuilder enemyTurn(@NonNull EnemyTurnListener enemyTurnListener) {
            playerBuilder.enemyTurnListener(enemyTurnListener);
            return this;
        }

        @Override
        public PlayerBuilder onWin(@NonNull WinListener winListener) {
            playerBuilder.winListener(winListener);
            return this;
        }

        @Override
        public PlayerBuilder onLoose(@NonNull LooseListener looseListener) {
            playerBuilder.looseListener(looseListener);
            return this;
        }

        public Game create() {
            Player player = playerBuilder.build();
            return new PvPGame(player);
        }

    }

    public class SecondPlayerBuilder implements Game.PlayerBuilder {
        Player.PlayerBuilder playerBuilder = Player.builder();


        @Override
        public SecondPlayerBuilder nickname(@NonNull String nickname) {
            playerBuilder.nickname(nickname);
            return this;
        }

        @Override
        public SecondPlayerBuilder playerTurn(@NonNull MyTurnListener myTurnListener) {
            playerBuilder.myTurnListener(myTurnListener);
            return this;
        }

        @Override
        public SecondPlayerBuilder enemyTurn(@NonNull EnemyTurnListener enemyTurnListener) {
            playerBuilder.enemyTurnListener(enemyTurnListener);
            return this;
        }

        @Override
        public SecondPlayerBuilder onIncorrectMove(@NonNull IncorrectPositionListener incorrectPositionListener) {
            playerBuilder.incorrectPositionListener(incorrectPositionListener);
            return this;
        }

        @Override
        public SecondPlayerBuilder onWin(@NonNull WinListener winListener) {
            playerBuilder.winListener(winListener);
            return this;
        }

        @Override
        public SecondPlayerBuilder onLoose(@NonNull LooseListener looseListener) {
            playerBuilder.looseListener(looseListener);
            return this;
        }

        public void start() {
            try {
                optionalPlayerB = Optional.of(playerBuilder.build());
                boardOptional = Optional.of(new Board(playerA, optionalPlayerB.get()));
                PvPGame.this.start();
            } catch (RemoteException e) {
                Throwables.propagate(e);
            }
        }
    }

}
