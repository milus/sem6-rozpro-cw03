package com.milus.ex03.server.core

import com.google.common.collect.Sets

class WinningPolicy {

    Optional<Player> findWinner(Map<Integer, Player> positions) {
        Set<Set<Integer>> winningSets = Sets.newHashSet()

        winningSets.add([0, 1, 2].toSet())
        winningSets.add([3, 4, 5].toSet())
        winningSets.add([6, 7, 8].toSet())

        winningSets.add([0, 3, 6].toSet())
        winningSets.add([1, 4, 7].toSet())
        winningSets.add([2, 5, 8].toSet())

        winningSets.add([0, 4, 8].toSet())
        winningSets.add([2, 4, 6].toSet())

        def playersWithPositions = positions.keySet().groupBy { positions.get(it) }

        def find = playersWithPositions.find { player, pos ->
            winningSets.any {
                pos.toSet().containsAll(it)
            }
        }

        return Optional.ofNullable(find ? find.key : null)
    }
}
