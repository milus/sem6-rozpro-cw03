package com.milus.ex03.server.core;

import com.milus.ex03.server.api.Position;
import com.milus.ex03.server.api.RMIBoard;
import com.milus.ex03.server.api.listener.*;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.Random;

public class Computer extends Player {

    public static final String NAME = "MILUS_COMPUTER";
    private static final long serialVersionUID = -4299138307940476845L;

    public Computer() {
        super(NAME, new Listener(), new Listener(), new Listener(), new Listener(), new Listener());
    }

    private static class Listener implements MyTurnListener, EnemyTurnListener, IncorrectPositionListener, WinListener, LooseListener, Serializable {

        private static final long serialVersionUID = 6569512813349746038L;

        @Override
        public Position getPosition(RMIBoard board) throws RemoteException {
            return new Position(new Random().nextInt(9));
        }

        @Override
        public void onEnemyTurn(RMIBoard board) throws RemoteException {

        }

        @Override
        public void onIncorrectPosition(Position position) throws RemoteException {

        }

        @Override
        public void onWin(RMIBoard board) throws RemoteException {

        }

        @Override
        public void onLoose(RMIBoard board) throws RemoteException {

        }
    }
}
