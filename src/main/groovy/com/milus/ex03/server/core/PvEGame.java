package com.milus.ex03.server.core;

import com.google.common.base.Throwables;
import com.milus.ex03.server.api.Board;
import com.milus.ex03.server.api.Position;
import com.milus.ex03.server.api.RMIBoard;
import com.milus.ex03.server.api.listener.*;
import lombok.NonNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rx.Observable;
import rx.schedulers.Schedulers;

import java.rmi.RemoteException;
import java.util.concurrent.Executors;

public class PvEGame extends Game {
    public static final Logger LOGGER = LoggerFactory.getLogger(PvEGame.class);

    private final RMIBoard board;
    private final Player player;
    private final Computer computer = new Computer();

    public PvEGame(@NonNull Player player) throws RemoteException {
        this.player = player;
        this.board = new Board(player, computer);
    }

    @Override
    public void start() {
        Observable.create(subscriber -> subscriber.onNext(board))
                .observeOn(Schedulers.from(Executors.newSingleThreadExecutor()))
                .map(b -> playerMove())
                .map(b -> computerMove())
                .map(b -> playerMove())
                .map(b -> computerMove())
                .map(b -> playerMove())
                .map(b -> computerMove())
                .map(b -> playerMove())
                .map(b -> computerMove())
                .map(b -> playerMove())
                .subscribe(b -> {
                    try {
                        finish();
                        Player winner = board.getWinner();
                        if (winner.equals(player)) {
                            player.onWin(board);
                        } else {
                            player.onLoose(board);
                        }
                    } catch (RemoteException e) {
                    }
                },
                        Throwable::printStackTrace);


    }

    @Override
    public boolean waitingForPlayers() {
        return false;
    }

    private RMIBoard playerMove() {
        try {
            if (board.isFinished()) {
                return board;
            }
        } catch (RemoteException e) {
            Throwables.propagate(e);
        }
        return doNextMove(player);
    }

    private RMIBoard computerMove() {
        try {
            if (board.isFinished()) {
                return board;
            }
        } catch (RemoteException e) {
            Throwables.propagate(e);
        }
        RMIBoard board = doNextMove(computer);
        try {
            player.onEnemyTurn(board);
        } catch (RemoteException e) {
            Throwables.propagate(e);
        }
        return board;
    }

    private RMIBoard doNextMove(Player player) {
        try {
            Position position = player.getPosition(board);
            while (!board.isMovePossible(player, position)) {
                player.onIncorrectMove(position);
                position = player.getPosition(board);
            }
            board.move(player, position);
        } catch (RemoteException e) {
            Throwables.propagate(e);
        }
        return board;
    }

    public static class Builder {
        public PlayerBuilder withPlayer() {
            return new PlayerBuilder();
        }
    }

    public static class PlayerBuilder implements Game.PlayerBuilder {
        Player.PlayerBuilder playerBuilder = Player.builder();

        @Override
        public PlayerBuilder nickname(@NonNull String nickname) {
            playerBuilder.nickname(nickname);
            return this;
        }

        @Override
        public PlayerBuilder playerTurn(@NonNull MyTurnListener myTurnListener) {
            playerBuilder.myTurnListener(myTurnListener);
            return this;
        }

        @Override
        public PlayerBuilder onIncorrectMove(IncorrectPositionListener incorrectPositionListener) {
            playerBuilder.incorrectPositionListener(incorrectPositionListener);
            return this;
        }

        @Override
        public PlayerBuilder enemyTurn(@NonNull EnemyTurnListener enemyTurnListener) {
            playerBuilder.enemyTurnListener(enemyTurnListener);
            return this;
        }

        @Override
        public PlayerBuilder onWin(@NonNull WinListener winListener) {
            playerBuilder.winListener(winListener);
            return this;
        }

        @Override
        public PlayerBuilder onLoose(@NonNull LooseListener looseListener) {
            playerBuilder.looseListener(looseListener);
            return this;
        }

        public Game create() {
            Player player = playerBuilder.build();
            try {
                return new PvEGame(player);
            } catch (RemoteException e) {
                Throwables.propagate(e);
                return null;
            }
        }
    }
}
