package com.milus.ex03.server.core.rmi;

import com.milus.ex03.server.api.GameType;
import com.milus.ex03.server.api.listener.*;
import lombok.NonNull;

import java.io.Serializable;
import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RMIGameBuilder extends Remote, Serializable {
    RMIGameBuilder type(GameType gameType) throws RemoteException;

    RMIGameBuilder myTurnListener(MyTurnListener listener) throws RemoteException;

    RMIGameBuilder enemyTurnListener(EnemyTurnListener listener) throws RemoteException;

    RMIGameBuilder incorrectPositionListener(IncorrectPositionListener listener) throws RemoteException;

    RMIGameBuilder winListener(WinListener listener) throws RemoteException;

    RMIGameBuilder looseListener(LooseListener listener) throws RemoteException;

    void go() throws RemoteException;
}
