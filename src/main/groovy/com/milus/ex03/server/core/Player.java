package com.milus.ex03.server.core;

import com.milus.ex03.server.api.*;
import com.milus.ex03.server.api.listener.*;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.io.Serializable;
import java.rmi.RemoteException;

@EqualsAndHashCode(of = "nickname")
@Builder
public class Player implements Serializable {
    private static final long serialVersionUID = 3055868304448003471L;
    @Getter
    private final String nickname;
    private final MyTurnListener myTurnListener;
    private final EnemyTurnListener enemyTurnListener;
    private final IncorrectPositionListener incorrectPositionListener;
    private final WinListener winListener;
    private final LooseListener looseListener;

    public Position getPosition(RMIBoard board) throws RemoteException {
        return myTurnListener.getPosition(board);
    }

    public void onEnemyTurn(RMIBoard board) throws RemoteException {
        enemyTurnListener.onEnemyTurn(board);
    }

    public void onIncorrectMove(Position position) throws RemoteException {
        incorrectPositionListener.onIncorrectPosition(position);
    }

    public void onWin(RMIBoard board) throws RemoteException {
        winListener.onWin(board);
    }

    public void onLoose(RMIBoard board) throws RemoteException {
        looseListener.onLoose(board);
    }
}
