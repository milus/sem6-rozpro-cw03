package com.milus.ex03.server.core;

import com.milus.ex03.server.api.listener.*;

public abstract class Game {
    private boolean isFinished = false;

    public boolean isFinished() {
        return isFinished;
    }

    protected void finish() {
        isFinished = true;
    }

    public abstract void start();

    public abstract boolean waitingForPlayers();

    public static PvEGame.Builder pve() {
        return new PvEGame.Builder();
    }

    public static PvPGame.Builder pvp() {
        return new PvPGame.Builder();
    }

    public interface PlayerBuilder {
        PlayerBuilder nickname(String nickname);

        PlayerBuilder playerTurn(MyTurnListener myTurnListener);

        PlayerBuilder enemyTurn(EnemyTurnListener enemyTurnListener);

        PlayerBuilder onIncorrectMove(IncorrectPositionListener incorrectPositionListener);

        PlayerBuilder onWin(WinListener winListener);

        PlayerBuilder onLoose(LooseListener looseListener);

    }
}
