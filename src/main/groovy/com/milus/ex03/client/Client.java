package com.milus.ex03.client;

import com.milus.ex03.server.api.*;
import com.milus.ex03.server.api.exception.ExistingUserException;
import com.milus.ex03.server.api.listener.*;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Random;
import java.util.Scanner;

@RequiredArgsConstructor
public class Client {
    public static final Logger LOGGER = LoggerFactory.getLogger(Client.class);

    private final String hostname;
    private final int port;
    private final String nickname;

    private MyTurnListener myTurnListener;
    private EnemyTurnListener enemyTurnListener;
    private IncorrectPositionListener incorrectPositionListener;
    private WinListener onWinListener;
    private LooseListener onLooseListener;

    private RMIServer server;

    private final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        if (args.length < 3) {
            System.err.println("Launch arguments: hostname port nickname");
            System.exit(1);
            return;
        }

        try {
            new Client(args[0], Integer.valueOf(args[1]), args[2]).onStart();
        } catch (RemoteException | MalformedURLException | NotBoundException e) {
            System.err.println("Remote exception. Exiting.");
            e.printStackTrace();
            System.exit(1);
        } catch (NumberFormatException e) {
            System.err.println("Invalid port number. Exiting.");
            System.exit(1);
        }
    }

    private void onStart() throws RemoteException, MalformedURLException, NotBoundException {
        server = (RMIServer) Naming.lookup(String.format("rmi://%s:%d/server", hostname, port));
        try {
            server.join(nickname);
        } catch (ExistingUserException e) {
            System.err.println("User with your nickname has already joined. Please pass another nickname");
            System.exit(1);
            return;
        }

        myTurnListener = this::selectAndMarkPosition;
        enemyTurnListener = this::onEnemyMove;
        incorrectPositionListener = this::incorrectPosition;
        onWinListener = this::onWin;
        onLooseListener = this::onLoose;

        initRmi();

        showTutorial();

        GameType type = chooseType();

        server.joinGame(nickname)
                .type(type)
                .myTurnListener(myTurnListener)
                .enemyTurnListener(enemyTurnListener)
                .incorrectPositionListener(incorrectPositionListener)
                .winListener(onWinListener)
                .looseListener(onLooseListener)
                .go();
        LOGGER.info("Game joined");
    }

    private GameType chooseType() {
        System.out.println("Please type 0 to start PVE game or 1 to start PVP game");
        while (scanner.hasNext()) {
            if (scanner.hasNextInt()) {
                int number = scanner.nextInt();
                if (number == 0) {
                    return GameType.PVE;
                } else if (number == 1) {
                    return GameType.PVP;
                }
            } else {
                scanner.next();
            }
            System.out.println("Incorrect number. Try again");
        }
        System.out.println("You didn't chose anything, so you have to play PVE game.");
        return GameType.PVE;
    }

    private void initRmi() throws RemoteException {
        myTurnListener = (MyTurnListener) UnicastRemoteObject.exportObject(myTurnListener, 0);
        enemyTurnListener = (EnemyTurnListener) UnicastRemoteObject.exportObject(enemyTurnListener, 0);
        incorrectPositionListener = (IncorrectPositionListener) UnicastRemoteObject.exportObject(incorrectPositionListener, 0);
        onWinListener = (WinListener) UnicastRemoteObject.exportObject(onWinListener, 0);
        onLooseListener = (LooseListener) UnicastRemoteObject.exportObject(onLooseListener, 0);
    }

    private void showTutorial() {
        System.out.println("I'll show you available position numbers");
        System.out.println("0|1|2");
        System.out.println("3|4|5");
        System.out.println("6|7|8");
    }

    private Position selectAndMarkPosition(RMIBoard board) {
        printBoard(board);
        System.out.println("Choose free position (from 0 to 8 inclusive)");
        while (scanner.hasNext()) {
            if (scanner.hasNextInt()) {
                return new Position(scanner.nextInt());
            } else {
                System.out.println(scanner.next() + " is not a number. Try again.");
            }
        }
        System.out.println("You haven't chosen any position. I'm sending random value");
        return new Position(new Random().nextInt(9));
    }

    private void onEnemyMove(RMIBoard board) {
        System.out.println("Enemy has chosen!");
    }

    private void printBoard(RMIBoard board) {
        System.out.println("Current board:");
        System.out.println(board.toString());
    }

    private void onWin(RMIBoard board) throws RemoteException {
        printBoard(board);
        System.out.println("Im winner!");
        server.disconnect(nickname);
        System.exit(0);
    }

    private void onLoose(RMIBoard board) throws RemoteException {
        printBoard(board);
        System.out.println("Im looser :-(");
        server.disconnect(nickname);
        System.exit(0);
    }

    private void incorrectPosition(Position position) {
        System.out.println(position.getValue() + " is taken.");
    }
}
